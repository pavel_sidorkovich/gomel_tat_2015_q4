package com.epam.tat.module8.test;

import com.epam.tat.module8.ui.page.yandexmail.LoginPage;
import com.epam.tat.module8.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class BaseLoginTest {

    protected WebDriver driver;

    private String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
    private String userPassword = "tat-123qwe"; // ACCOUNT

    @BeforeClass
    public void prepareBrowser() {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);
    }
}
