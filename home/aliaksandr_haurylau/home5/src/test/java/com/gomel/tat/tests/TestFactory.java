package com.gomel.tat.tests;

import org.testng.annotations.Factory;

public class TestFactory {

    @Factory
    public Object [] createInstance() {
          return new Object[] { new PatternTest("*1251*.txt", 1),new PatternTest("*folder4/*.*", 1)};
    }

}
