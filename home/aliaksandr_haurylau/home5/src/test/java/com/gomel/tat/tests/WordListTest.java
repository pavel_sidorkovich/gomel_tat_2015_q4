package com.gomel.tat.tests;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.*;
import java.io.File;
import java.io.IOException;



public class WordListTest {

    @BeforeSuite(alwaysRun = true)
    public void setUp() throws Exception {

        String source = "./src/test/resources/testFixture.zip";
        String destination = "./src/test/resources/";
        try {
            ZipFile zipFile = new ZipFile(source);
            zipFile.extractAll(destination);
        } catch (ZipException e) {
            e.printStackTrace();
        }

    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() throws Exception {
        File dir = new File("./src/test/resources/testfolder");
        try {
            FileUtils.deleteDirectory(dir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}