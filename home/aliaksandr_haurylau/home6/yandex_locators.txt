        Google Search Page

1. GoodleQueryInputFieldLocator			//*[@id='gs_htif0']					#lst_ib
2. GoogleSearchButtonLocator			//input[@name='btnK']				[name=btnK]
3. GoogleIAmLuckyButtonLocator			//input[@name='btnI']				[name=btnI]
4. GoogleAllTenSearchResultsLinksLocator 	(//div[@class='g'] /a)|(//div[@class='g']//h3/a)    .g h3>a,
                                                                                           div#imagebox_bigimages>div>a äëÿ èçîáðàæåíèé
5. GoofleNthLetterOInGooogleLocator		.//*[@id='nav']/tbody/tr/td[N+1]			tbody tr td:nth-child(N+1)

        Yandex Mail Login Page

6. YandexMailLoginInputFieldLocator 		//input[@name='login']			[name='login']
7. YandexMailPasswordInputFieldLocator		//input[@name='passwd'] 		[name='passwd'] 
8. YandexMailEnterButtonLocator 		.//button[contains(@class,'start')]	.nb-button._nb-action-button.nb-group-start

        Yandex Mail Postbox Page
9.
   YandexMailIncomingFolderLocator		//*[@href='#inbox']			a.b-folders__folder__link[href='#inbox']
   YandexMailOutcomingFolderLocator		//*[@href='#sent'] 			a.b-folders__folder__link[href='#sent'] 
   YandexMailSpamFolderLocator			//*[@href='#spam'] 			a.b-folders__folder__link[href='#spam']
   YandexMailDeletedFolderLocator		//*[@href='#trash'] 			a.b-folders__folder__link[href='#trash']
   YandexMailDraftsFolderLocator		//*[@href='#draft'] 			a.b-folders__folder__link[href='#draft']

10.
   YandexMailLetterComposeLocator		//*[@href='#compose']			        .b-toolbar__item.b-toolbar__item_compose
   YandexMailLetterCheckNewMailLocator		//*[contains(@class,'item-check-mail')]	        a[class*='check-mail']
   YandexMailLetterForwardLocator		//*[contains(@class,'item-forward')]		a[class*='item-forward ']
   YandexMailLetterDeleteLocator		//*[contains(@class,'item-delete')]		a[class*='item-delete']
   YandexMailLetterMarkSpamLocator		//*[contains(@class ,'item_spam')]		a[class*='item_spam']
   YandexMailLetterMarkNoSpamLocator		//*[contains(@class ,'item-not-spam')]	        a[class*='item-not-spam']
   YandexMailLetterMarkReadedLocator		//*[contains(@class ,'item_mark-as-read')]	a[class*='item_mark-as-read']
   YandexMailLetterMoveIntoFolderLocator	//*[contains(@class ,'item_infolder')]          a[class*='item_infolder']         

11. ExampleOfYandexUniqeMailMessageLocator 	//*[@href='#message/2560000002353645351']	a[href="#message/2300000009219456510"]

        Yandex Disk Page

12. YandexDiskUploadButton			input class="button__attach"			.button__attach
13. YandexDiskUploadedFileLocator		.//div[@title='filename.ext']			div[title='filename.ext'] 
14. YandexDiskDeleteButtonLocator 		.//button[@title='Óäàëèòü']			button[title='Óäàëèòü']
15. YandexDiskTrashboxLocator			.//div[@title='Êîðçèíà']			div[title='Êîðçèíà']
16. YandexDiskRestoreButtonLocator 	 .//button[@data-click-action='resource.restore']	button[data-click-action='resource.restore'


