package com.gomel.tat.home7;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.home.tat.home7.enums.Browsers;
import com.home.tat.home7.utils.CreateInstanceOfWebDriver;

public class BaseMailTest {
	public static final String BASE_URL = "https://mail.yandex.by";
	public static final By LOGIN_INPUT_LOCATOR = By.name("login");
	public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
	public static final String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
	public static final String userPassword = "tat-123qwe"; // ACCOUNT
	public static final By COMPOSE_BUTTON_LOCATOR = By
			.xpath("//a[@href='#compose']");
	public static final By TO_INPUT_LOCATOR = By
			.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
	public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
	public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
	public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
	public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
	public static final By SENTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#sent']");
	public static final By DRAFTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#draft']");
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='compose.delete']");
	public static final By TRASHBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#trash']");
	public static final By PERMANENT_DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='delete']");
	public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
	public static final String mailTo = "tat-test-user@yandex.ru"; // ENUM
	public static final String mailSubject = "test subject";
	public static final String mailContent = "mail content";
	public WebDriver driver;

	@BeforeClass(description = "Prepare browser")
	public void prepareBrowser() throws MalformedURLException {
		// driver = CreateInstanceOfWebDriver.getDriver(Browsers.CHROME);
	}

	@AfterClass(description = "Close browser")
	public void clearBrowser() {
		// driver.quit();
	}

	public void loginMail(Browsers browser, String login, String password)
			throws MalformedURLException {
		driver = CreateInstanceOfWebDriver.getDriver(browser);
		driver.get(BASE_URL);
		WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
		loginInput.sendKeys(login);
		WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
		passInput.sendKeys(password);
		passInput.submit();
	}

	public void sendMail(String mailTo, String mailSubject, String mailContent) {
		createDraftMail(mailTo, mailSubject, mailContent);
		WebElement sendMailButton = driver
				.findElement(SEND_MAIL_BUTTON_LOCATOR);
		sendMailButton.click();

	}

	public WebElement waitForElementIsClickable(By locator) {
		new WebDriverWait(driver, 5000).until(ExpectedConditions
				.elementToBeClickable(locator));
		return driver.findElement(locator);
	}

	public WebElement waitForDisappear(By locator) {
		new WebDriverWait(driver, 5000).until(ExpectedConditions
				.invisibilityOfElementLocated(locator));
		return driver.findElement(locator);
	}

	public void createDraftMail(String mailTo, String mailSubject,
			String mailContent) {
		WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
		composeButton.click();
		WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
		toInput.sendKeys(mailTo);
		WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
		subjectInput.sendKeys(mailSubject);
		WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
		mailContentText.sendKeys(mailContent);

	}

}
