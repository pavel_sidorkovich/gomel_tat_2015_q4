package com.gomel.tat.home5;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.codehaus.plexus.util.DirectoryScanner;

import com.gomel.tat.home5.exceptions.InvalidBaseDirectoryException;
import com.gomel.tat.home5.utils.CommonFileUtils;

public class WordList {
	public static final String BASE_DIRECTORY = "d:/TAT/TAT_2015_Q4/gomel_tat_2015_q4_h4";
	public static final String FILE_NAME_PATTERN = "**/**/*home3/hello.*";
	public static final String WRONG_DIRECTORY_MESSAGE = "Base directory is wrong";
	public static final String STATISTICS_IS_EMPTY = "There is no data for statistics";
	private List<String> values = null;

	public static void main(String[] args) {
		WordList wordList = new WordList();
		wordList.getWords(BASE_DIRECTORY, FILE_NAME_PATTERN);
		wordList.printStatistics();
	}

	public void getWords(String baseDirectory, String fileNamePattern) {
		File baseDir = new File(BASE_DIRECTORY);
		try {
			if (!baseDir.exists() || baseDir.isFile()) {
				throw new InvalidBaseDirectoryException(WRONG_DIRECTORY_MESSAGE);
			}
			List<File> files = getFiles(baseDirectory, fileNamePattern);
			values = parseFilesToListStrings(files);
			files.clear();
		} catch (InvalidBaseDirectoryException e) {
			e.printStackTrace();
		}
	}

	public void printStatistics() {
		if (values != null) {
			Map<String, Integer> mapStatistics = fillMapStatistics(values);
			printMapStatistics(mapStatistics);
			mapStatistics.clear();
		} else {
			System.out.println(STATISTICS_IS_EMPTY);
		}
	}

	public List<File> getFiles(String baseDirectory, String fileNamePattern) {
		List<File> files = new ArrayList<File>();
		String[] childPaths = getChildPaths(baseDirectory, fileNamePattern);

		for (String childPath : childPaths) {
			files.add(new File(baseDirectory, childPath));
		}
		return files;
	}

	public List<String> parseFilesToListStrings(List<File> files) {
		final String SPLITTER = "'?\"?\\s+\"?'?";
		List<String> listStrings = new ArrayList();
		String fileContent;
		String[] splitedContent;

		for (File file : files) {
			try {
				String encoding = CommonFileUtils.getEncoding(file
						.getAbsolutePath());
				fileContent = FileUtils.readFileToString(file, encoding);
				splitedContent = fileContent.split(SPLITTER);
				listStrings.addAll(Arrays.asList(splitedContent));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return listStrings;
	}

	public String[] getChildPaths(String baseDirectory, String fileNamePattern) {
		DirectoryScanner scaner = new DirectoryScanner();
		scaner.setIncludes(new String[] { fileNamePattern });
		scaner.setBasedir(baseDirectory);
		scaner.setCaseSensitive(false);
		scaner.scan();
		String[] childPaths = scaner.getIncludedFiles();
		return childPaths;
	}

	public Map<String, Integer> fillMapStatistics(List<String> values) {
		final String WORD_VALID_PATTERN = ".*[a-zA-Zа-яА-Я]+.*";
		Map<String, Integer> mapStat = new HashMap<String, Integer>();

		for (String value : values) {
			if (value.matches(WORD_VALID_PATTERN)) {
				int frequency = Collections.frequency(values, value);
				mapStat.put(value, frequency);
			}
		}
		return mapStat;
	}

	public void printMapStatistics(Map<String, Integer> mapStatistics) {
		final String OUTPUT_FORMAT = "Word: %18s    appearences: %s";

		for (Map.Entry<String, Integer> entry : mapStatistics.entrySet()) {
			System.out.println(String.format(OUTPUT_FORMAT, entry.getKey(),
					entry.getValue()));
		}
	}
}
