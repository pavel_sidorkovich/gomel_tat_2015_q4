package com.gomel.tat.home5.exceptions;

public class InvalidBaseDirectoryException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidBaseDirectoryException() {
		super();
	}

	public InvalidBaseDirectoryException(String message) {
		super(message);
	}

}
