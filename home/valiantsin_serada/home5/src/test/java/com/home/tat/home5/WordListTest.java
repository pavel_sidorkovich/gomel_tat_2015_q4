package com.home.tat.home5;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.gomel.tat.home5.WordList;

public class WordListTest {
	public WordList wordList;
	public final static String TEMP_DIRECTORY = "/temp";
	public final static String SOURCE_DIRECTORY = "/home/valiantsin_serada/home3";
	public String SOURCE_PATH = wordList.BASE_DIRECTORY.concat(TEMP_DIRECTORY);
	public List<String> testStringList;
	public Map<String, Integer> testMap = new HashMap<String, Integer>();
	private File tempDir = new File(
			wordList.BASE_DIRECTORY.concat(TEMP_DIRECTORY));
	private File srcDir = new File(
			wordList.BASE_DIRECTORY.concat(SOURCE_DIRECTORY));

	@BeforeTest
	public void beforeTest() {
		try {
			FileUtils.forceMkdir(tempDir);
			FileUtils.copyDirectoryToDirectory(srcDir, tempDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		wordList = new WordList();
		String[] testStrings = new String[] { "Dandelion", "Wine", "the",
				"Evening", "News", "A", "Farewell", "to", "Arms", "Just",
				"the", "Way", "It", "Is" };
		testStringList = Arrays.asList(testStrings);
		fillTestMap();
	}

	@AfterTest
	public void afterTest() {
		try {
			FileUtils.deleteDirectory(tempDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		testMap.clear();
	}

	private void fillTestMap() {
		testMap.put("Farewell", 1);
		testMap.put("to", 1);
		testMap.put("A", 1);
		testMap.put("the", 2);
		testMap.put("Is", 1);
		testMap.put("News", 1);
		testMap.put("Evening", 1);
		testMap.put("It", 1);
		testMap.put("Arms", 1);
		testMap.put("Dandelion", 1);
		testMap.put("Way", 1);
		testMap.put("Just", 1);
		testMap.put("Wine", 1);

	}
}
