package com.gomel.tat.home5.test.testing.runner;

import com.gomel.tat.home5.test.testing.listeners.ListenerFailedTestMethod;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {

        TestNG tng = new TestNG();
        tng.addListener(new ListenerFailedTestMethod());

        XmlSuite suite = new XmlSuite();
        suite.setName("WordCountCheck");
        List<String> files = new ArrayList();
        files.addAll(new ArrayList<String>() {{
            add("./src/test/resources/suites/words.xml");
        }});
        suite.setSuiteFiles(files);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);
        tng.run();
    }
}
