package com.gomel.tat2015.home4;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class WordList {


    private Map<String, Integer> values = new HashMap<>();

    public void getWords(String baseDirectory, String fileNamePattern) {
        File file = new File(baseDirectory);
        searchFiles(file, fileNamePattern);
    }

    public void searchFiles (File file, String fileNamePattern) {

        if (!file.isDirectory()) {
            if (isMaskSuitable(fileNamePattern, file)) {
                parseFile(file);

            }

        }

        if (file.isDirectory()) {
            try {
                File[] children = file.listFiles();

                for (File child : children) {
                    searchFiles(child, fileNamePattern);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void parseFile(File file){

        try {

            FileReader fileReader = null;

            fileReader = new FileReader(file);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                StringTokenizer tokenizer = new StringTokenizer(line, " !\"\'?.,*\t():-");
                while (tokenizer.hasMoreTokens()) {
                    String token = tokenizer.nextToken();
                    if (values.containsKey(token)) {
                        values.put(token, values.get(token) + 1);
                    } else {
                        values.put(token, 1);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printStatistics() {
        for (Map.Entry<String, Integer> value : values.entrySet()) {
            System.out.println(value.getKey() + ": " + value.getValue());
        }
    }

    public boolean isMaskSuitable (String mask, File file){

        return file.getName().matches(mask);
    }

    public static void main(String args[]) {
        WordList wordList = new WordList();
        wordList.getWords("d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\src\\main\\java\\com\\gomel\\tat2015\\home4\\gomel_tat_2015_q4\\home\\", "hello.txt");
        wordList.printStatistics();
    }
}
