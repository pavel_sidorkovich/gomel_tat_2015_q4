package com.gomel.tat2015.home4;

import org.testng.annotations.*;

import java.io.IOException;

public class TestWrongFileNameThrowsException extends BaseWordListTest {

    private String fileNamePattern;

    @Factory(dataProvider = "fileNameValues")
    public TestWrongFileNameThrowsException(String fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    @Test (expectedExceptions = IOException.class)
    public void checkWrongFileNameThrowsException() throws IOException{
        wordList.getWords(tempDirPath, fileNamePattern);
    }

    @DataProvider(name = "fileNameValues")
    public static Object[][] startFileNameValuesForCheck() {
        return new Object[][]{
                {"*hoe///.txt"},
                {"home3/hello.txt"},
                {"*hello///"},
                {"*home//=\\=hello*"}
        };
    }
}
