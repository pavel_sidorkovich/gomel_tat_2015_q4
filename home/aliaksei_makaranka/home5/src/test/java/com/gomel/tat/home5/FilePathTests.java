package com.gomel.tat.home5;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FilePathTests extends BaseWordListTests{

    @Test(expectedExceptions = NullPointerException.class, description = "Checking file for null path", groups = "first") //expectedExceptions = NullPointerException.class,
    public void checkNullFilePath() {
        wordList.getWords("D:\\EPAM\\TestFolderRepository", null);
        Assert.assertNotNull(wordList);
    }

    @Test(description = "Checking file for empty path", dependsOnGroups = "first")
    public void checkEmptyFilePath() {
        wordList.getWords("D:\\EPAM\\TestFolderRepository", "");
        Assert.assertNotNull(wordList);
    }

    @Test(description = "Checking file for wrong path")
    public void checkWrongFilePath() {
        wordList.getWords("D:\\EPAM\\TestFolderRepository", "hello1.txt");
        Assert.assertNotNull(wordList);
    }
}