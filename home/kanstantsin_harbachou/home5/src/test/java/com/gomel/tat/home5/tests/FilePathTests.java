package com.gomel.tat.home5.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FilePathTests extends BaseWordListTests {

    @Test(description = "Checking file for null path", groups = "Null and empty group")
    public void checkNullFilePath() {
        System.out.println("checkNullFilePath");
        checkTime();
        wordList.getWords("e:/gomel_tat_2015_q4/home", null);
        Assert.assertEquals(true, wordList.getValues().isEmpty());
    }

    @Test(description = "Checking file for empty path", groups = "Null and empty group")
    public void checkEmptyFilePath() {
        System.out.println("checkEmptyFilePath");
        checkTime();
        wordList.getWords("e:/gomel_tat_2015_q4/home", "");
        Assert.assertEquals(true, wordList.getValues().isEmpty());
    }

    @Test(description = "Checking file for wrong path")
    public void checkWrongFilePath() {
        System.out.println("checkWrongFilePath");
        checkTime();
        wordList.getWords("e:/gomel_tat_2015_q4/home", "home/hello1.txt");
        Assert.assertEquals(true, wordList.getValues().isEmpty());
    }
}
