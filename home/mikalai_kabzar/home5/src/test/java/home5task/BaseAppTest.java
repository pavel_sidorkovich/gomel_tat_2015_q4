package home5task;

import home4taskFilesForTest.WordList;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;

import java.util.Date;

import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.TreeMap;

/**
 * Created by Shaman on 16.12.2015.
 */
public class BaseAppTest {

    TreeMap<String, Integer> mapOne = new TreeMap<String, Integer>();
    TreeMap<String, Integer> mapTwo = new TreeMap<String, Integer>();
    String testFolder = "d:\\TAT\\UnitTest\\";
    String fileMask = "h?l*.txt";

    protected void checkTime() {
        System.out.println("Current time: " + new Date(System.currentTimeMillis()));
    }

    protected void sleep() {
        try {
            Thread.sleep(1);                 //1000 milliseconds is one second. Now I don`t need sleep
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    @BeforeSuite
    public void beforeSuite() {
        mapOne.put("every", 4);
        mapOne.put("test", 4);
        mapOne.put("should", 3);
        mapOne.put("check", 3);
        mapOne.put("some", 2);
        mapOne.put("condition", 2);
        mapOne.put("using", 1);
        mapOne.put("asserts", 1);

        mapTwo.put("до", 5);
        mapTwo.put("миллионов", 5);
        mapTwo.put("за", 4);
        mapTwo.put("срубленную", 4);
        mapTwo.put("елку", 4);
        mapTwo.put("контролеры", 3);
        mapTwo.put("выставили", 2);
        mapTwo.put("на", 2);
        mapTwo.put("дорогах", 2);
        mapTwo.put("страны", 1);
        mapTwo.put("сотни", 1);
        mapTwo.put("постов", 1);

        System.out.println("BeforeSuite test passed");
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("BeforeClass test passed");
    }

    @BeforeGroups
    public void beforeGroup() {
        System.out.println("BeforeGroups test passed");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("BeforeMethod test passed");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("AfterMethod test passed");
    }

    @AfterGroups
    public void afterGroup() {
        System.out.println("AfterGroups test passed");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("AfterClass test passed");
    }

    @AfterSuite
    public void afterSuite() {
        mapOne.clear();
        mapTwo.clear();
        System.out.println("AfterSuite test passed");
    }
}
