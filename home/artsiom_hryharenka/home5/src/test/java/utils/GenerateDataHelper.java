package utils;

import org.apache.commons.io.FileUtils;
import java.io.*;
import java.util.Random;

public class GenerateDataHelper {
    final static String BASE_DIRECTORY_NAME = "base test folder";
    final static String TEST_DIRECTORIES_NAME = "test folder";
    final static String TEST_FILE_NAME = "test file.txt";
    final static String TEST_TEXT = "_+-.,!@#$%^&*();\\/|<>\"' word";


    public static String getBaseDirectoryPath() {
        return "d:\\" + BASE_DIRECTORY_NAME + "\\";
    }

    public static String getFileName() {
        return TEST_FILE_NAME;
    }

    public static void createBaseDirectory() {
        for (int i = 0; i < 10; i++) {
            try {
                File folder = new File(getBaseDirectoryPath() + TEST_DIRECTORIES_NAME + i + "\\" + TEST_DIRECTORIES_NAME);
                folder.mkdirs();

                File fileTXT = new File(folder, getFileName());

                FileWriter writer = new FileWriter(fileTXT, false);
                writer.write(TEST_TEXT + " " + generateUniqueWord());
                writer.flush();
                writer.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex.getMessage());
            }
        }
    }

    private static String generateUniqueWord() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            char c = chars[random.nextInt(chars.length)];
            builder.append(c);
        }
        String uniqueWord = builder.toString();
        return uniqueWord;
    }

    public static void removeDirectory() {
        try {
            FileUtils.deleteDirectory(new File(getBaseDirectoryPath()));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
