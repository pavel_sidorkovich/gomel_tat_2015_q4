package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

public class GetFileListTest extends BaseWordListTest {
    @Test(groups = "get words")
    public void getFileList() {

        Collection<File> expectedFile = new ArrayList<File>();
        expectedFile.add(new File("D:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\home\\pavel_potapov\\home3\\hello.txt"));
//        System.out.println(expectedFile);
        Collection<File> file = wordList.getFileList("D:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\home\\pavel_potapov\\home3", "glob:hello.txt");
        Assert.assertEquals(file, expectedFile, "Invalid file");
    }
}