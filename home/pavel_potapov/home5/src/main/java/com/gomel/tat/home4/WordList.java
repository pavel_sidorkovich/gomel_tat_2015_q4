package com.gomel.tat.home4;

import java.io.File;

import org.apache.commons.io.FileUtils;



import java.util.*;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;


public class WordList {
    private List<String> values = new ArrayList<String>();
    private HashMap<String, Integer> originalWords = new HashMap<String, Integer>();
    public List<String> getWords(String baseDirectory, String fileNamePattern) {
        Collection<File> files = getFileList(baseDirectory, fileNamePattern);
        String files_content = filesToString(files);
        String[] file_content_arr = wordsInFileContent(files_content);
        values = removeEmptyStrings(file_content_arr);
        return values;
    }

    public void printStatistics() {

        final String DISPLAY_FORMAT = "%15s  - %s";
        System.out.println(String.format(DISPLAY_FORMAT, "Words", "Quantity"));
        originalWords = wordsCounter(values);
        for (String word : originalWords.keySet()) {
            System.out.println(String.format(DISPLAY_FORMAT, word, originalWords.get(word)));
        }
    }
    public Collection<File> getFileList(String baseDirectory, String fileNamePattern) {
        final Collection<File> files = new ArrayList<File>();
        final PathMatcher helloTxtMatcher = FileSystems.getDefault().getPathMatcher(fileNamePattern);
        try {
        Files.walkFileTree(Paths.get(baseDirectory), new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path path, BasicFileAttributes attributes) throws IOException {
                if (!attributes.isDirectory() && helloTxtMatcher.matches(path.getFileName())) {

                    String path_absolute = path.toAbsolutePath().toString();

                    files.add(new File(path_absolute));

                }

                return FileVisitResult.CONTINUE;
            }
        });
        } catch (IOException exc) {
            exc.printStackTrace();
        }

//        System.out.println(files);
        return files;
    }
    public String filesToString(Collection<File> files) {
        String files_content = "";
        for (File file : files) {
            try {
                files_content += FileUtils.readFileToString(file, "Cp1251") + " ";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return files_content;
    }
    public String[] wordsInFileContent(String input) {
        String[] output = input.toLowerCase().split("[^а-яa-z]");
        return output;
    }
    public List<String> removeEmptyStrings(String[] file_content_arr) {
        List<String> words = new ArrayList<String>();
        for (String word : file_content_arr) {
            if (!word.isEmpty()) {
                words.add(word);
            }
        }
        return words;
    }

    public HashMap<String, Integer> wordsCounter(List<String> words) {
        HashMap<String, Integer> originalWords = new HashMap<String, Integer>();
        for (String word : values) {
            int i = originalWords.containsKey(word) ? originalWords.get(word) : 0;
            originalWords.put(word, ++i);
        }
        return originalWords;
    }
}

