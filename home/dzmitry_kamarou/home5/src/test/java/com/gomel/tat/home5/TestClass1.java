package com.gomel.tat.home5;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.*;

public class TestClass1 extends BaseTest {
    @Test(dependsOnMethods = "testMethod3", groups = "2")
    public void testMethod1() {
        File file = new File("some_name");
        wordList.getWords(file.getAbsolutePath(), "*hello.txt");
        if (file.exists()) {
            Assert.assertEquals(wordList.message, "", wordList.message);
        } else {
            Assert.assertEquals(wordList.message, "Not exists directory.", wordList.message);
        }
    }

    @Test(dependsOnMethods = "testMethod3", groups = "1")
    public void testMethod2() {
        wordList.getWords("", "");
        wordList.getWords("C:/", "");
        wordList.getWords("", "*hello.txt");
        Assert.assertEquals(wordList.message, "Empty data.", wordList.message);
    }

    @Test(expectedExceptions = NullPointerException.class, groups = "1")
    public void testMethod3() {
        wordList.getWords(null, null);
        wordList.getWords("C:/", null);
        wordList.getWords(null, "*hello.txt");
        System.out.println(wordList.message);
        Assert.assertEquals(wordList.message, "Null pointer.", "NullPointerException was caught.");
    }
}
