package com.gomel.tat.home5;

import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.io.*;

public class TestClass2 extends BaseTest {
    private List<String> list;
    String dir, pattern;

    @Test(groups = "1")
    public void testMethod1() {
        String baseDirectory = new File("").getAbsolutePath();
        WorkUtilClass.createEmptyFiles(new File(baseDirectory + "/TempDir"));
        Assert.assertEquals(wordList.allText, "", "No matches found.");
        System.out.println("No matches found check.");
    }

    @Test
    public void testMethod2() {
        int commonWordCount = 0;
        int countDirectories = 1 + (int) (Math.random() * ((10 - 5) + 1));
        System.out.println("Random count directories=" + countDirectories);
        for (int i = 0; i < countDirectories; i++) {
            File newDir = WorkUtilClass.createDirectory(tempDir, Integer.toString(i + 1));
            int countFiles = 1 + (int) (Math.random() * ((10 - 5) + 1));
            for (int j = 0; j < countFiles; j++) {
                File newFile = WorkUtilClass.createFile(newDir, Integer.toString(j + 1) + "hello.txt");
                int countCheckWords = 1 + (int) (Math.random() * ((10 - 5) + 1));
                String text = "";
                for (int k = 0; k < countCheckWords; k++) {
                    text += "check ";
                }
                try {
                    FileWriter fileWriter = new FileWriter(newFile);
                    fileWriter.write(text);
                    fileWriter.close();
                } catch (IOException e) {
                    System.out.println("IOException message." + e);
                    exception = e;
                }
                commonWordCount += countCheckWords;
            }
        }
        wordList.getWords(baseDirectory, "*hello.txt");
        wordList.printStatistics();
        int count = wordList.values.size();
        System.out.println("count=" + count);
        System.out.println("commonWordCount=" + commonWordCount);
        Assert.assertEquals(commonWordCount, count, "Count words exception message.");
    }

    @Test(groups = "1")
    public void testMethod3() {
        System.out.println("testMethod3");
        wordList.getWords(dir, pattern);
    }

    @Factory(dataProvider = "dataProvider1")
    public TestClass2(String dir, String pattern) {
        this.dir = dir;
        this.pattern = pattern;
    }

    @DataProvider(name = "dataProvider1")
    public static Object[][] dataProviderMethod() {
        return new Object[][]{{"C:/some", "*hi.txt"},
                {"C:/some1", "aloha.txt"},
        };
    }
}



