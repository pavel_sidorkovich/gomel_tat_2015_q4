package com.gomel.tat.home4;

/**
 * Main class Home4
 *
 * @author Pavel Mihayeu
 * @version 0.2 15 Dec 2015
 */

public class Home4 {
    public static void main(String[] args) {
        WordList Runner = new WordList();
        Runner.run();
    }
}
