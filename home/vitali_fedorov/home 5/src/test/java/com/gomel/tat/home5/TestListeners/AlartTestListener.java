package com.gomel.tat.home5.TestListeners;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Vitali on 20.12.2015.
 */
public class AlartTestListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult testRes) {
        File bugReportWordList = new File("D:/bugReportWordList.txt");
        FileWriter fileWriter = null;
        try {
            if (!bugReportWordList.exists()) {
                bugReportWordList.createNewFile();
            }
            fileWriter = new FileWriter(bugReportWordList, true);
            fileWriter.write("\r\n************************ Failure ************************");
            fileWriter.write("\r\n=> Test: " + testRes.getTestClass().getName());
            fileWriter.write("\r\n=> Method: " + testRes.getName());
            fileWriter.write("\r\n**********************************************************");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
