package com.gomel.tat.home5;


import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import java.io.IOException;


/**
 * Created by Vitali on 19.12.2015.
 */

import static org.testng.Assert.assertEquals;

public class TestPattern extends BaseWordListTest {

    String fileNamePattern;

    @Factory(dataProvider = "fileNameValues")
    public TestPattern(String fileName) {
        fileNamePattern = fileName;
    }

    @Test
    public void TestFolderNamePattern() throws IOException {
        wordList.getWords(startDir, fileNamePattern);
        assertEquals(wordList.getValues().get("suffered"), new Integer(2), "Invalid folder name pattern.");

    }


    @DataProvider(name = "fileNameValues")
    public static Object[][] fileNameValuesForCheck() {
        return new Object[][]{
                {"\\\\home ?3\\\\readme.txt"},
                {"\\\\home ?3\\\\hello.txt"},
               {"\\\\home ?3\\\\crybaby.txt"},
                {"\\\\home ?3\\\\h*.txt"},
//
        };
    }
}
