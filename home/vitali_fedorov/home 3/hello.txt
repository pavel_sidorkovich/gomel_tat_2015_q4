Vitali Fedorov
1. "The Name of the Wind" and "The Wise Man's Fear" by Patrick Rothfuss
2. "Alice in Wonderland" by  Lewis Carroll (in origin)
3. "The Chronicles of Siala" by Aleksei Pehov
4. "The Art of War" by Sun Tzu
5. "Flowers for Algernon" by Daniel Keyes