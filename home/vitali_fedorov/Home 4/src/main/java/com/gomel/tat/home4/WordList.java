package com.gomel.tat.home4;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class WordList {
    public static void processFilesFromFolder(File folder) throws IOException {
        File[] folderEntries = folder.listFiles(); //�������� ������ ���� ������ �����
        for (File entry : folderEntries) {
            if (entry.isDirectory()) {
                processFilesFromFolder(entry); //���� ���� ��� �����, �� ���� � ����
                continue;
            } else {
                Main.subMain(entry);// ����� ��� ������� ���� � ������������ ���
            }
        }
    }


    private static List<String> values; // ���� � ������ ���������� - �������

    public static void setValues(List<String> values) { //������
        WordList.values = values;
    }

    public static List<String> getValues() { //������
        return values;
    }
    // public void getWords(String baseDirectory, String fileNamePattern) {
    // your code goes here
    //  }

    public static void printStatistics(Map<String, Integer> map) {   //����� ������ �����+��� ����������
        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            System.out.println(pair.getKey() + " " + pair.getValue());
        }
    }

    public static Map<String, Integer> countWords(ArrayList<String> list) //��� ��� �������� ���������� ����
    {
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        for (String word : list) {
            if (result.containsKey(word)) {
                result.put(word, result.get(word) + 1);
            } else {
                result.put(word, 1);
            }
        }
        return result;
    }


    public static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {

        // Convert Map to List
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

}
