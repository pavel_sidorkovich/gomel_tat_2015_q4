package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

public class FilesToStringTest extends GetFileListTest {
    @Test(groups = "get words", dependsOnMethods = "getFileList")
    public void filesToString() {
        String files_content = wordList.filesToString(files);
        String expectedFilesContent = "\"Tales of power\" Karlos Kastaneda \r\n" +
                "\"Autumn of the Patriarch\" Gabriel Garsia Markes\r\n" +
                "\"Martian chronicles\" Ray Bradbury\r\n" +
                "\"The Little Prince\"  Antoine de Saint-Exupery\r\n" +
                "\"Chapayev and Void\" Victor Pelevin Pavel Potapov\r\n" +
                "\r\n" +
                " Terry Pratchett \"Discworld\"\r\n" +
                " Duglas Adams \"The Hitchhiker's Guide to the Galaxy\"\r\n" +
                " Jonathan Maberry \"������� ��������\" � \"�����\"\r\n" +
                " Hasekura Isuna \"Volchica i prjanosti\"\r\n" +
                " Eoin Colfer \"Artemis Faul\" 1. Iain Banks 'The Wasp Factory'.\r\n" +
                "2. Chuck Palahniuk 'Survivor'.\r\n" +
                "3. Jerome Salinger 'Teddy'.\r\n" +
                "4. Joseph Heller 'Catch-22'.\r\n" +
                "5. Kodo Sawaki 'To you'. ";
        Assert.assertEquals(files_content, expectedFilesContent, "Invalid result array");
    }
}
