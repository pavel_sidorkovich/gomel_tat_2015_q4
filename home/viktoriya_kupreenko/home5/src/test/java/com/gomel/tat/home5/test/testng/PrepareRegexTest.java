package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

public class PrepareRegexTest extends BaseWordListTest {

    @Test(groups = "get words")
    public void prepareRegex() {
        String pattern = wordList.prepareRegex("������ ���� * \\");
        Assert.assertEquals(pattern, "������ ���� .* \\\\", "Invalid pattern");
    }

    @Test(expectedExceptions = AssertionError.class)
    public void badPattern() {
        throw new RuntimeException();
    }
}
