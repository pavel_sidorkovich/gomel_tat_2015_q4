package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

public class WordsInFileContentTest extends BaseWordListTest {
    @Test(groups = "get words")
    public void wordsInFileContent() {
        String[] file_content_arr = wordList.wordsInFileContent("������ ���� 1");
        String[] expectedArray = {"������", "����"};
        Assert.assertEquals(file_content_arr, expectedArray, "Invalid result array");
    }
}
