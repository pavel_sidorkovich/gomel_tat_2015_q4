package com.gomel.tat.home5.test.testng.listeners;

import org.apache.commons.io.FileUtils;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.IOException;

public class CustomListener extends TestListenerAdapter{
    private String path = "D:\\EPAM\\Automation\\dev\\GOMEL_TAT_2015_Q4\\home\\viktoriya_kupreenko\\home5\\failed_tests.txt";
    private File failedTests = new File(path);

    @Override
    public void onTestFailure(ITestResult tr) {
        String data = "****Failed test****\r\nTest name:  "+tr.getTestClass() +
                "\r\nMethod name:  " + tr.getName() +
                "\r\nException message:  " +tr.getThrowable().toString() +
                "\r\nStacktrace:\r\n";

        for(StackTraceElement ste : tr.getThrowable().getStackTrace()){
            data += ste.toString() + "\r\n";
        }

        try {
            FileUtils.writeStringToFile(failedTests,data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult tr) {
        System.out.println(tr.getName()+ "--Test method skipped\n");
    }

    @Override
    public void onTestSuccess(ITestResult tr) {
        System.out.println(tr.getName()+ "--Test method success\n");
    }

}
